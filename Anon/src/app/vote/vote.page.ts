import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import {OrganizacionesService}from '../services/organizaciones.service'

@Component({
  selector: 'app-vote',
  templateUrl: './vote.page.html',
  styleUrls: ['./vote.page.scss'],
})
export class VotePage implements OnInit {
  opciones= [];
  constructor(private route: ActivatedRoute, private org:OrganizacionesService, private storage: Storage) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.org.buscarpreguntasopciones(id).subscribe(data=>{
      this.opciones=data["data"]
      console.log(data["data"]);
    });
  }

  next(opciones){
    this.storage.set('IdEvent', opciones.event_id ).then(
      () => {
        
      },
      error => console.error('Error storing item, error')
    );
    this.storage.set('Pregunta',opciones.question ).then(
      () => {
        
      },
      error => console.error('Error storing item, error')
    );
    this.storage.set('Opciones',opciones.options ).then(
      () => {
        
      },
      error => console.error('Error storing item, error')
    );
    this.storage.set('IdOpciones',opciones.options_id ).then(
      () => {
        
      },
      error => console.error('Error storing item, error')
    );
  }

  votar(event_id, question, opciones){
  }
}
