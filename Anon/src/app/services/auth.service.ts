import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { EnvService } from './env.service';

import { Storage } from '@ionic/storage';
import { AlertService } from './alert.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user_id: number;
  token: any;
  constructor(private http: HttpClient,
    private storage: Storage,
    private env: EnvService,
    private alertService: AlertService) { }
  httpParams = new HttpParams();
  login(Email, Contrasena) {
    if (Email == "" || Contrasena == "") {
      this.alertService.presentToast("Faltan Datos", "warning");
    }
    else {
      this.httpParams = this.httpParams.set("Email", Email.toString());

      this.httpParams = this.httpParams.set("Contrasena", Contrasena.toString());

      return this.http.get(this.env.API_URL + 'user/login', { params: this.httpParams })
    }

  }

}
