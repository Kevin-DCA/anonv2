import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { EnvService } from './env.service';

import { Storage } from '@ionic/storage';
import { AlertService } from './alert.service';

@Injectable({
  providedIn: 'root'
})
export class OrganizacionesService {
  httpParams = new HttpParams();
  constructor(private http: HttpClient,
    private storage: Storage,
    private env: EnvService,
    private alertService: AlertService) { }
   org(){
    this.httpParams =this.httpParams.set("Cedula", "1");
    return this.http.get(this.env.API_URL + 'user/buscarorg', { params: this.httpParams })
   }

   event(){
    this.httpParams =this.httpParams.set("Id", "1");
    return this.http.get(this.env.API_URL + 'user/buscareve', { params: this.httpParams })
   }

   buscarpreguntasopciones(Id){
    this.httpParams =this.httpParams.set("IdEvento", Id);
    return this.http.get(this.env.API_URL + 'user/buscarpregyop', { params: this.httpParams })
   }
 
}
