import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { EnvService } from './env.service';

import { Storage } from '@ionic/storage';
import { AlertService } from './alert.service';
@Injectable({
  providedIn: 'root'
})
export class VoteService {

  constructor(private http: HttpClient,
    private storage: Storage,
    private env: EnvService,
    private alertService: AlertService) { }
}
