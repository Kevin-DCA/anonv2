import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Platform } from '@ionic/angular';
import {AuthService}from '../services/auth.service'
import { AlertService } from 'src/app/services/alert.service';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {

  password: string;

  constructor(public router: Router, private afAuth: AngularFireAuth,
    private platform: Platform,private authService: AuthService,
    private alertService: AlertService,private storage: Storage) { }
 
  name = '';
  email = '';
  ngOnInit() {}

  doLogin()
  {
    let color :any;
    let code : any;
    
    this.authService.login(this.email,this.password).subscribe(
      data => {
       if(data["data"]!=undefined)
     { var aux=data["data"][0];
       this.res(aux.users_mail,aux.users_name,aux.users_id);}
       else
       this.alertService.presentToast("Usuario incorrecto", "warning");
      },
      error => {
        this.alertService.presentToast("Usuario o contraseña incorrecto", "warning");
      },
      () => {
        if (code == 1) {

        }
      }   
    )
    
  }
  loginGoogle() {
    if (this.platform.is('capacitor')) {
      this.loginGoogleAndroid();
    } else {
      this.loginGoogleWeb();
    }
  }

  async loginGoogleAndroid() {
    
  }

  async loginGoogleWeb() {
    const res = await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    const user = res.user;
    if (res.user != null){
      this.name = user.displayName;
      this.email = user.email;
      this.res(this.email,name,9999);
      //Redireccionar a inicio
      this.router.navigate(['/menu']);
    }
    
   
  }
  res(correo,nombre,id){
    this.storage.set('correo',correo ).then(
      () => {
        console.log('Token Stored');
      },
      error => console.error('Error storing item, error')
    );
    if(id!=9999)
    this.storage.set('Id',id ).then(
      () => {
        console.log('Token Stored');
      },
      error => console.error('Error storing item, error')
    );
    this.storage.set('nombre',nombre ).then(
      () => {
        console.log('Token Stored');
      },
      error => console.error('Error storing item, error')
    );
this.router.navigate(['/menu']);
  }
}
