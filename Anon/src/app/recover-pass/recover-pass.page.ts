import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recover-pass',
  templateUrl: './recover-pass.page.html',
  styleUrls: ['./recover-pass.page.scss'],
})
export class RecoverPassPage implements OnInit {

  constructor(private alertService: AlertService,public router: Router) { }

  ngOnInit() {
  }
  recover(){this.alertService.presentToast("La informacion sera enviada a su correo", "success");
  this.router.navigate(['/login']);
}
  
}
