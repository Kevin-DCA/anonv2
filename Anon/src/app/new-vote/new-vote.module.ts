import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewVotePageRoutingModule } from './new-vote-routing.module';

import { NewVotePage } from './new-vote.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewVotePageRoutingModule
  ],
  declarations: [NewVotePage]
})
export class NewVotePageModule {}
