import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewVotePage } from './new-vote.page';

const routes: Routes = [
  {
    path: '',
    component: NewVotePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewVotePageRoutingModule {}
