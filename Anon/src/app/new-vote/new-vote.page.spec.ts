import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewVotePage } from './new-vote.page';

describe('NewVotePage', () => {
  let component: NewVotePage;
  let fixture: ComponentFixture<NewVotePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVotePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewVotePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
