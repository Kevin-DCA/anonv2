import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { MenuPageModule } from "./menu/menu.module";

const routes: Routes = [
  { path: 'login', loadChildren: () => import('../app/login-form/login-form.module').then( m => m.LoginFormModule)},
 
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'sig-in',
    loadChildren: () => import('./sig-in/sig-in.module').then( m => m.SigInPageModule)
  },
  {
    path: 'recover-pass',
    loadChildren: () => import('./recover-pass/recover-pass.module').then( m => m.RecoverPassPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'vote/:id',
    loadChildren: () => import('./vote/vote.module').then( m => m.VotePageModule)
  },
  {
    path: 'new-vote',
    loadChildren: () => import('./new-vote/new-vote.module').then( m => m.NewVotePageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
