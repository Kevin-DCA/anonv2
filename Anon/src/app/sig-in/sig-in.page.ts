import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AlertController } from '@ionic/angular'

@Component({
  selector: 'app-sig-in',
  templateUrl: './sig-in.page.html',
  styleUrls: ['./sig-in.page.scss'],
})
export class SigInPage implements OnInit {
  name: string;
  email: string;

  password: string;
  cpassword: string;

  constructor(public alertController: AlertController, private router: Router) { }

  ngOnInit() {
  }

  async presentAlert(message){
    const alert = await this.alertController.create({
      header: 'Mensaje',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
    let result = await alert.onDidDismiss();
    console.log(result);
  }

  doSigin(){
    if(this.password !== this.cpassword){
      this.presentAlert('Las contraseñas no coinciden, vuelva a intentarlo');
    return false;
    } else{
      //Proceso de Guardar el usuario

      //Luego que el usuario a sido guardado exitosamente:
      this.presentAlert('¡Registro exitoso!');
      //Redireccionar a la página de login
      this.router.navigate(['/login']);

      return true;
    }
  }

}
