// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};
export const firebaseConfig = {
  apiKey: "AIzaSyChzLyvxJR76-_t9Wgl2vT_eqvKHsu901g",
  authDomain: "anon-e05cb.firebaseapp.com",
  databaseURL: "https://anon-e05cb.firebaseio.com",
  projectId: "anon-e05cb",
  storageBucket: "anon-e05cb.appspot.com",
  messagingSenderId: "191600206345",
  appId: "1:191600206345:web:8e96f411f3b587b427ced8",
  measurementId: "G-2251FXTKXM"
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
